"use strict";

let idCounter = 1;

function Product(name,price){
    this.name = name;
    this.price = price;
    this.selected = false;
    this.id = idCounter++;
}
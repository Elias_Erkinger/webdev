"use strict";

function getSelectedProductsFromSelect(select){
    let productHtmlSelectedOptions = select.selectedOptions;
    return getProductListFromHtmlOptions(productHtmlSelectedOptions);
}
function getProductListFromHtmlOptions(productHtmlOptionList){
    let productList = [];
    for(let productHtmlOption of productHtmlOptionList){
        let product = getProductByID(productHtmlOption.getAttribute("productID"));
        productList.push(product);
    }
    return productList;
}

function clearProductSelects(){
    let availAbleProductSelect = document.getElementById(PRODUCT_AVAILABLE_SELECT);
    let selectionOfProductSelect = document.getElementById(PRODUCT_SELECTION_SELECT);

    availAbleProductSelect.innerHTML = "";
    selectionOfProductSelect.innerHTML = "";
}

function insertProductListToSelects(productList){
    let availAbleProductSelect = document.getElementById(PRODUCT_AVAILABLE_SELECT);
    let selectionOfProductSelect = document.getElementById(PRODUCT_SELECTION_SELECT);
    
    for(let product of productList){
        let htmlOption = createHtmlProductOption(product);
        if(product.selected == false){    
            availAbleProductSelect.append(htmlOption);
        }else{
            selectionOfProductSelect.append(htmlOption);
        }
    }
}
function createHtmlProductOption(product){
    let option = document.createElement("option");
    option.innerHTML = product.name;
    option.setAttribute("productID",product.id);
    return option;
}



const RESULT = "result";
function showResult(result){
    let resultHtml = document.getElementById(RESULT);
    resultHtml.style.display = "inline";
    resultHtml.innerHTML = result;
}
function hideResult(){
    let resultHtml = document.getElementById(RESULT);
    resultHtml.style.display = "none";
}

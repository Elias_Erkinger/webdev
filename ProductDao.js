"use strict";

const availableProducts = [];
availableProducts.push(new Product("Red Strat DG",2000));
availableProducts.push(new Product("Black Strat",3900000));
availableProducts.push(new Product("Sunburst Relic Tele", 4000));
availableProducts.push(new Product("Angular",1234));


const productSelection = [];

let productList = [];


function getProductByID(id){
    let returnProduct = null;
    productList = availableProducts.concat(productSelection);
    productList.forEach(product => {
        if(product.id == id){
            returnProduct = product;
        };
    });

    return returnProduct;
}

function getProductList(){
    productList = availableProducts.concat(productSelection);
    return productList;
}

function getProductSelection(){
    return productSelection;
}
function getAvailableProducts(){
    return availableProducts;
}

function shiftProductsToSelection(products){
    removeProductsAndPushIt(products,availableProducts,productSelection);
}
function shiftProductsToAvailable(products){
    removeProductsAndPushIt(products,productSelection,availableProducts);
}
function removeProductsAndPushIt(products,removeThere,pushHere){
    for(let product of products){
        let index = removeThere.indexOf(product);
        if(index > -1){
            removeThere.splice(index,1);
            pushHere.push(product);
        }
    }
}
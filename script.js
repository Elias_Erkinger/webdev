"use strict";

function sortAvailableProducts(){
    let productList = getAvailableProducts();
    sortListByTextProperty(productList,(product)=>product.name);
}

function shiftSelectedProdToSelection(){
    let availableProductSelect = document.getElementById(PRODUCT_AVAILABLE_SELECT);
    let productList = getSelectedProductsFromSelect(availableProductSelect);

    // set property of each product to be selected
    setSelectedPropertyOfEachProduct(productList,true);
    // set product dao properly
    shiftProductsToSelection(productList);
}

function shiftSelectedProdToAvailable(){
    let selectionSelect = document.getElementById(PRODUCT_SELECTION_SELECT);
    let productList = getSelectedProductsFromSelect(selectionSelect);

    setSelectedPropertyOfEachProduct(productList,false);
    shiftProductsToAvailable(productList);
}
function setSelectedPropertyOfEachProduct(products,selected){
    for(let product of products){
        product.selected = selected;
    }
}
function updateSelects(){
    clearProductSelects();
    insertProductListToSelects(getProductList());
}

function calculateTotal(){
    let sum = 0;
    let productList = getProductSelection();

    for(let product of productList){
        sum += product.price;
    }

    showResult(sum);
}

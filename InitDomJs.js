"use strict";

const PRODUCT_AVAILABLE_SELECT = "availableProductSelect";
const PRODUCT_SELECTION_SELECT = "selectionOfProductSelect";
const SHIFT_TO_AVAILABLE_BTN = "shiftToAvailble";
const SHIFT_TO_SELECTED_BTN = "shiftToSelected";
const CALCULATE_TOTAL_BUTTON = "calculateTotal";
const SORT_AVAILABLE_PRODUCTS_BTN = "sortAvailableProducts";

function init(){
    updateSelects();
    initShiftButtons();

    document.getElementById(CALCULATE_TOTAL_BUTTON).addEventListener("click",calculateTotal);
    document.getElementById(SORT_AVAILABLE_PRODUCTS_BTN).addEventListener("click",sortAvailableProducts);
    document.getElementById(SORT_AVAILABLE_PRODUCTS_BTN).addEventListener("click",updateSelects);
}
function initShiftButtons(){
    let shiftToAvailbleBtn = document.getElementById(SHIFT_TO_AVAILABLE_BTN);
    let shiftToSelectedBtn = document.getElementById(SHIFT_TO_SELECTED_BTN);

    shiftToAvailbleBtn.innerHTML = "<<";
    shiftToSelectedBtn.innerHTML = ">>";

    shiftToSelectedBtn.addEventListener("click", shiftSelectedProdToSelection);
    shiftToAvailbleBtn.addEventListener("click", shiftSelectedProdToAvailable);

    let addClickListenerToBothShiftBtn = (listener) => {
        shiftToAvailbleBtn.addEventListener("click",listener);
        shiftToSelectedBtn.addEventListener("click",listener);
    };

    addClickListenerToBothShiftBtn(updateSelects);
    addClickListenerToBothShiftBtn(hideResult);
}
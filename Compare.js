"use strict";

function sortListByTextProperty(list,getText){
    let len = list.length;
    let swapped;
    do {
        swapped = false;
        for (let i = 0; i < len - 1; i++) {
            let iText = getText(list[i]);
            let nextText = getText(list[i + 1]);

            if (compareText(iText,nextText) == 1) {
                let tmp = list[i];
                list[i] = list[i + 1];
                list[i + 1] = tmp;
                swapped = true;
            }
        }
    } while (swapped);
    return list;
}
function compareText(text1,text2){
    let shortestLength = text1.length < text2.length ? text1.length : text2.length;
    for(let i = 0; i < shortestLength; i++){
        if(text1[i] == text2[i]){
            continue;
        }
        if(text1[i] > text2[i]){
            return 1;
        }else{
            return -1;
        }
    }

    if(text1.length == text2.length){
        return 0;
    }
    if(text1.length == shortestLength){
        return -1;
    }else{
        return 1;
    }
}

function testCompareText(){
    let success;
    let compare = compareText("mars","marshall");
    success = assert(-1,compare);

    compare = compareText("marshall","mars");
    success = assert(1,compare);

    compare = compareText("roger","roger");
    success = assert(0,compare);

    if(success){
        console.log("Everything ok.");
    }
}
function assert(expected,actual){
    if(actual === expected){
        return true;
    }else{
        console.log("Test failed.");
        return false;
    }
}